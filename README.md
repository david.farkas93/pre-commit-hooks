# pre-commit-hooks

A set of useful pre-commit hooks.

## Usage

```yaml
repos:
  - repo: https://gitlab.com/david.farkas93/pre-commit-hooks
    rev: master
    hooks:
      - id: isort
      - id: mypy
      - id: pylint
      - id: pytest
```

Add required dependencies.

```bash
poetry add --dev "isort<5" mypy pylint pytest pytest-cov
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
